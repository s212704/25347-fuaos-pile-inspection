'''Begin of the script to extract the diameter of piles in a harbour, by using the Opencv library and Stereo matching to get a Disparity Map followed by a Depth map
and finally the extraction of the diamweter, by using the bounding box feature of Opencv'''
# Importing the necessary libraries
import cv2
import json
import numpy as np
import matplotlib.pyplot as plt
import random as rng
import os
import argparse
import sys
import open3d as o3d

#Obtain the Rectification parameters 
#Function Inputs are the Camera Intrinsics, Distortion Coefficients, Image Size, Rotation and Translation between the Cameras
def stereoRectify(cam_matrix_left, left_dist_coeffs, cam_matrix_right, right_dist_coeffs, img_size, rotation, translation):
    global R1, R2, P1, P2, Q
    (R1, R2, P1, P2, Q, roi1, roi2) = cv2.stereoRectify(cam_matrix_left, left_dist_coeffs, cam_matrix_right, right_dist_coeffs, img_size, rotation, translation, None, None, None, None, None, cv2.CALIB_ZERO_DISPARITY, 0)
    return R1, R2, P1, P2, Q

#Apply the Rectification to the Images by using the initUndistortRectifyMap function which returns the rectification map for the images 
#Inputs are the Rectification Matrices, Projection Matrices, Image Size, Camera Intrinsics and Distortion Coefficients
def imageRectify(R1, R2, P1, P2, img_size,cam_matrix_left, left_dist_coeffs, cam_matrix_right, right_dist_coeffs):
    map1x, map1y = cv2.initUndistortRectifyMap(cam_matrix_left, left_dist_coeffs, R1, P1, img_size, cv2.CV_32FC1)
    map2x, map2y = cv2.initUndistortRectifyMap(cam_matrix_right, right_dist_coeffs, R2, P2, img_size, cv2.CV_32FC1)
    return map1x, map1y, map2x, map2y

#Function to denoise the background of the images by using the Morphological Operations and the divide function of Opencv to get the binary images
#Inputs are the Left and Right Images 
#Outputs are the denoised binary images
#get structuring element is obtained by using the getStructuringElement function of Opencv with a Rectangular Kernel of 8x8 pixels
# https://stackoverflow.com/questions/62042172/how-to-remove-noise-in-image-opencv-python
def background_denoise(imagesL, imagesR):
    #Denoising of the Left images]
    se=cv2.getStructuringElement(cv2.MORPH_RECT , (8,8))
    bg=cv2.morphologyEx(imagesL, cv2.MORPH_DILATE, se)
    out_grayL=cv2.divide(imagesL, bg, scale=255)
    out_binaryL=cv2.threshold(out_grayL, 150, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU )[1] 
    #Denoising of the Right images
    se=cv2.getStructuringElement(cv2.MORPH_RECT , (8,8))
    bg=cv2.morphologyEx(imagesR, cv2.MORPH_DILATE, se)
    out_grayR=cv2.divide(imagesR, bg, scale=255)
    out_binaryR=cv2.threshold(out_grayR, 150, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU )[1] 
    return out_binaryL, out_binaryR

#Function to get the Disparity Map by using the StereoBM algorithm of Opencv 
#Returns the disparity map for each image
def stereoMatch(i):
    out_binaryL = []
    out_binaryR = []
    out_binaryL, out_binaryR = background_denoise(imagesL[i], imagesR[i])
    # cv2.imshow('Left Image', out_binaryL)
    # cv2.imshow('Right Image', out_binaryR)

    cv2.imwrite(f'./Output/Left_Image_{i}.jpg', out_binaryL)
    cv2.imwrite(f'./Output/Right_Image_{i}.jpg', out_binaryR)
    #Stereo Matching using the StereoBM algorithm
    # numdiaprities must be divisible by 16
    stereo = cv2.StereoBM_create(numDisparities=160, blockSize=5)
    disparity_i = stereo.compute(out_binaryL, out_binaryR)
    # plt.imshow(disparity_i,cmap='jet')
    # plt.colorbar()
    # plt.title(f'Disparity Map {i}')
    # # plt.savefig(f'./Output/Disparity_{i}.jpg') 
    # plt.show()
    return disparity_i

#Display the disparity map
def depthMap(disparity, Q, threshold_value,i):
    depth_drawing = cv2.reprojectImageTo3D(disparity[i], Q)
    # Set a threshold value to segment the depth map
    depth_map_thresholded = cv2.threshold(depth_drawing.astype(np.uint8), threshold_value, 255, cv2.THRESH_BINARY)[1]
    depth_map_thresholded =cv2.cvtColor(depth_map_thresholded, cv2.COLOR_BGR2GRAY)
    # Convert the thresholded depth map to grayscale
    depth_map_gray = cv2.cvtColor(depth_drawing, cv2.COLOR_BGR2GRAY)
    # plt.imshow(depth_map_gray, cmap='gray')
    # plt.colorbar()
    # plt.title(f'Depth Map {i}')
    # # plt.savefig(f'./Output/Depth_Map_{i}.jpg')
    # plt.show()
    return depth_map_gray, depth_map_thresholded

#Function to find the Contours of the Depth Map and draw them on the depth map image
#Returns the image with the contours and the sorted contours
#using the RETr_TREE mode to get all the contours and the CHAIN_APPROX_SIMPLE to get the contour points
#The contours are sorted by area in descending order
#The contours are drawn on the image with a random color
#The function returns the image with the contours and the sorted contours
def find_contours(depth_map_filtered):
    #find Contours for each depth map
    contours, hierarchy = cv2.findContours(depth_map_filtered, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Sort contours by area in descending order
    contours_sorted = sorted(contours, key=cv2.contourArea, reverse=True)
    # Draw contours
    drawing_depth = np.zeros((depth_map_filtered.shape[0], depth_map_filtered.shape[1], 3), dtype=np.uint8)
    for i in range(0,len(contours),1):
        
            color = (rng.randint(0,256), rng.randint(0,256), rng.randint(0,256))
            cv2.drawContours(drawing_depth, contours, i, color, 2, cv2.LINE_8, hierarchy, 0)

    # cv2.imshow('Contours', drawing_depth)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # cv2.imwrite(f'./Output/Contours_{i}.jpg', drawing_depth)
    return drawing_depth,contours_sorted

#Function to calculate the Diameter of the Piles by using the bounding box feature of Opencv
#Returns the largest distance in the x direction of the bounding box
#The bounding box is drawn on the image
#The distance is calculated and displayed on the image
def calculate_diameter(contours_test, drawing_test):
    #first input is the image with the contours and the second input is the sorted contours
    #second input is selecting the second largest contour
    #third is -1 means all contours are drawn
    #fourth is the color of the contour in BGR
    #fifth is the thickness of the contour -1 means filled
    cv2.drawContours(drawing_test, [contours_test[1]], -1, (0, 255, 0), 2)
    # Get the bounding rectangle of the second largest contour
    x, y, w, h = cv2.boundingRect(contours_test[1])
    # Draw the bounding rectangle on the image
    cv2.rectangle(drawing_test, (x, y), (x + w, y + h), (255, 0, 0), 2)
    # Calculate the largest distance in the x direction
    largest_distance_x = w
    print("Largest distance in the x direction of the second largest contour:", largest_distance_x)
    # Calculate the end points of the line
    start_point = (x, y + h // 2)
    end_point = (x + w, y + h // 2)
    # Convert distance to string
    distance_str = f'{largest_distance_x} mm'
    # Put the text on the image
    cv2.putText(drawing_test, distance_str, start_point, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
    # Draw a straight line indicating the distance
    cv2.line(drawing_test, start_point, end_point, (0, 0, 255), 2)
    # Display the image with filtered contours
    # cv2.imshow('Estimation of diameter of the Pile', drawing_test)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # cv2.imwrite(f'./Output/Pile_diameter_New{i}.jpg', drawing_test)
    return largest_distance_x


'''Creating a Point Cloud from the Depth Map'''
def generate_pointcloud_method2(depth_file):

    rows, cols = depth_file.shape
    c, r = np.meshgrid(np.arange(cols), np.arange(rows), sparse=True)
    valid = (depth_file > 0) & (depth_file < 255)
    z = np.where(valid, depth_file / 256.0, np.nan)
    x = np.where(valid, z * (c - self.cx) / self.fx, 0)
    y = np.where(valid, z * (r - self.cy) / self.fy, 0)
    return np.dstack((x, y, z))


#Custom function to display the disparity map
def custom_draw_geometry_with_custom_fov(pcd, fov_step):
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.add_geometry(pcd)
    ctr = vis.get_view_control()
    print("Field of view (before changing) %.2f" % ctr.get_field_of_view())
    ctr.change_field_of_view(step=fov_step)
    print("Field of view (after changing) %.2f" % ctr.get_field_of_view())
    vis.run()
    vis.destroy_window()

'''Importing of the Stereo Calibration data and the images of the stereo camera'''
#Reading the calibration file of the stereo Camera
# with open('./oak_2024-04-08-10-52-07_0_converted_2024-04-09-14-35-58-camchain.json', 'r') as f:
#     calibration_data = json.load(f)

#Fletchers Calibration Files:
with open('./calibration-left-right.json', 'r') as f:
    calibration_data = json.load(f)

# Extract calibration parameters for left camera (cam0)
left_intrinsics = np.array(calibration_data['cam0']['intrinsics'])
left_dist_coeffs = np.array(calibration_data['cam0']['distortion_coeffs'])
# Extract calibration parameters for right camera (cam1)
right_intrinsics = np.array(calibration_data['cam1']['intrinsics'])
right_dist_coeffs = np.array(calibration_data['cam1']['distortion_coeffs'])
# Extract relative pose between the two cameras
rotation = np.array([l[0:3] for l in calibration_data["cam1"]["T_cn_cnm1"]][0:3])
translation = np.array([l[3] for l in calibration_data["cam1"]["T_cn_cnm1"]][0:3])

#Transfering the Camera intrinsics to right format into the 3x3 Camera Intrinsics Matrix
cam_matrix_left = np.array([[left_intrinsics[0], 0, left_intrinsics[2]],[0, left_intrinsics[1], left_intrinsics[3]],[ 0, 0, 1]])
cam_matrix_right = np.array([[right_intrinsics[0], 0, right_intrinsics[2]],[0, right_intrinsics[1], right_intrinsics[3]],[ 0, 0, 1]])

"""Selected Frames of the Stereo Camera Video stored in a list of Timestamps to load the Frames into the Script"""

#Timestamps of extracted Frames of the Stereo Camera Video 
timestamps =["00:02:15","00:02:34","00:02:37","00:03:25","00:03:47","00:03:50","00:03:58","00:04:15","00:04:30","00:05:15","00:05:25","00:06:02","00:06:05","00:06:26","00:06:48","00:06:58","00:07:04","00:07:39","00:07:45","00:07:53","00:07:57","00:08:10","00:08:15","00:08:20","00:08:25","00:08:29","00:08:34","00:08:36","00:08:39"]

"""Removing the Timestamps, which have to much noise and are not suitable for the analysis of the Piles Diameter"""

timestamps = [timestamps[i] for i in range(len(timestamps)) if i not in (3, 9, 11, 25, 27, 28)]


#Defining a Variable to load multiple images note that python starts at 0
# frames = 8
frames = len(timestamps)  # Assuming the number of frames is equal to the number of timestamps

#Creating an Array to store the images 
imagesL = []
imagesR = []

# index = [1,2,3,5,6,7,8,10,12,13,14,15,16,17,18,19,20,21,22,23,24,26]

# # List all files in the folder
# frames_left = './Frames-Left/Frames-Timestamps'
# frames_right = './Frames-Right/Frames-Timestamps'
# files_left = os.listdir(frames_left)
# files_right = os.listdir(frames_right)

# # Read each image using cv2.imread
# imagesL = [cv2.imread(os.path.join(frames_left, file)) for file in files_left]
# imagesR = [cv2.imread(os.path.join(frames_right, file)) for file in files_right]


# #Reading the images and storing them in the Array imagesL and imagesR as grayscale images
# for i in range(1,frames):
#     imgL = cv2.imread(f'./Frames-Left/Frames-Timestamps/frameout_left_{}_{}_%03d.jpg',cv2.IMREAD_GRAYSCALE)
#     imagesL.append(imgL)
#     imgR = cv2.imread(f'./Frames-Right/Frames-Timestamps/frameout_right_{}_{}_%03d.jpg',cv2.IMREAD_GRAYSCALE)
#     imagesR.append(imgR)

'''Loading all of the frames and storing them into ImageL and ImageR arrays for further processing'''

for index, timestamps in enumerate(timestamps, start=1):
    output_pattern_left = "frameout_left_{}_{}_001.jpg"
    output_filename_left = output_pattern_left.format(index, timestamps .replace(":", "_"))
    imgL = cv2.imread(f'./Frames-Left/Frames-Timestamps/{output_filename_left}', cv2.IMREAD_GRAYSCALE)
    # cv2.imshow('Left Image', imgL)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    cv2.imwrite(f'./Output/LeftImage_{index}.jpg', imgL)
    imagesL.append(imgL)

    output_pattern_right = "frameout_right_{}_{}_001.jpg"
    output_filename_right = output_pattern_right.format(index, timestamps.replace(":", "_"))
    imgR = cv2.imread(f'./Frames-Right/Frames-Timestamps/{output_filename_right}', cv2.IMREAD_GRAYSCALE)
    cv2.imwrite(f'./Output/RightImage_{index}.jpg', imgR)
    imagesR.append(imgR)

#Obtain Image size for the Rectification used as input for the StereoRectify function
img_size = (imagesL[0].shape[1], imagesL[0].shape[0])

#Variable for Length of the Array of imported Images for running over the images in a loop
len_images = len(imagesL)


'''Start of the Stereo Matching to get the Disparity Map'''
#Creating the Variables for the Rectification and Projection Matrices 
R1 = np.zeros((3,3))
R2 = np.zeros((3,3))
P1 = np.zeros((3,4))
P2 = np.zeros((3,4))
Q = np.zeros((4,4))


'''Rectification of the Images'''

R1, R2, P1, P2, Q = stereoRectify(cam_matrix_left, left_dist_coeffs, cam_matrix_right, right_dist_coeffs, img_size, rotation, translation)
map1x, map1y, map2x, map2y = imageRectify(R1, R2, P1, P2, img_size,cam_matrix_left, left_dist_coeffs, cam_matrix_right, right_dist_coeffs)
for i in range(0,len_images):
    cv2.remap(imagesL[i], map1x, map1y, cv2.INTER_LANCZOS4)
    cv2.remap(imagesR[i], map2x, map2y, cv2.INTER_LANCZOS4)
    cv2.fastNlMeansDenoising(imagesL[i], None, 10.0, 7, 21)
    cv2.fastNlMeansDenoising(imagesR[i], None, 10.0, 7, 21)

disparity = []
for i in range(0,len_images):
    disparity_i = stereoMatch(i)
    disparity.append(disparity_i)
    # plt.imshow(disparity_i,cmap='jet')
    # plt.colorbar()
    # plt.title(f'Disparity Map {i}')
    # plt.show()
    # plt.savefig(f'./Output/Disparity_{i}.jpg')
print(disparity)


'''Creating and Preprocessing of the Depth Map to extract the Diameter of the Piles'''

depth_map=[]
depth_map_filtered = []
for i in range(0,len_images):
    depth_map_gray, depth_map_thresholded = depthMap(disparity, Q,100,i)
    # plt.imshow(depth_map_gray, cmap='gray')
    # plt.colorbar()
    # plt.title(f'Depth Map {i}')
    # plt.show()
    # plt.savefig(f'./Output/Depth_Map_{i}.jpg')
    depth_map_blurred = cv2.medianBlur(depth_map_thresholded, 7)
    _, depth_map_thresh = cv2.threshold(depth_map_blurred, 50, 255, cv2.THRESH_BINARY)
    # cv2.imshow('Thresholded Depth Map', depth_map_thresh)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # cv2.imwrite(f'./Output/Thresholded_Depth_Map_{i}.jpg', depth_map_thresh)
    depth_map.append(depth_map_gray)
    depth_map_filtered.append(depth_map_thresh.astype(np.uint8))



contour_map = []
contours_sorted_array = []
for i in range(0,len_images):
    drawing_depth,contours_sorted  = find_contours(depth_map_filtered[i])
    contour_map.append(drawing_depth)
    contours_sorted_array.append(contours_sorted)

    
'''Start of the Calculation of the Diameter of the Piles'''

diameters = []
for i in range(0,len_images):
    # drawing_test = contour_map[i].copy()
    drawing_test = imagesL[i].copy()
    contours_test = contours_sorted_array[i].copy()
    largest_distance_x = calculate_diameter(contours_test, drawing_test)
    diameters.append(largest_distance_x)

diameters.pop(2 )
#Calculate the Mean Value of the obtained Diameters
mean_diameter = np.mean(diameters)
std_deviation = np.std(diameters)
print(f'The Mean Diameter of the Piles is {mean_diameter} mm')
print(f'The Standard Deviation of Diameter of the Piles is {std_deviation}')





"""Creating a Point Cloud from the Depth Map"""

#create a point cloud from the depth map

# pcd = []
# for i in range(0,len_images):
#     point_cloudi = depth_map[i].copy()
#     point_cloudi = point_cloudi.reshape(-1, point_cloudi.shape[-1])
#     point_cloudi = point_cloudi[~np.isinf(point_cloudi).any(axis=1)]
#     pcl = o3d.geometry.PointCloud()
#     pcl.points = o3d.utility.Vector3dVector(point_cloudi)
#     o3d.visualization.draw_geometries([pcl])
#     pcd.append(pcl)

# for i in range(0,len_images):
#     point_cloud = cv2.reprojectImageTo3D(disparity[i], Q)
#     point_cloud = point_cloud.reshape(-1, point_cloud.shape[-1])
#     point_cloud = point_cloud[~np.isinf(point_cloud).any(axis=1)]
#     pcl = o3d.geometry.PointCloud()
#     pcl.points = o3d.utility.Vector3dVector(point_cloud)
#     o3d.visualization.draw_geometries([pcl])
# Loop through each pixel in the image  
# h,w = imagesL[0].shape
# depth= depth_map[0]
# f_x = cam_matrix_left[0, 0] 
# f_y = cam_matrix_left[1, 1]
# u0 = cam_matrix_left[0, 2]
# v0 = cam_matrix_left[1, 2]
# for v in range(h):    
#   for u in range(w):    
#     # Apply equation in fig 4
#     x = (u - u0) * depth[v, u] / f_x
#     y = (v - v0) * depth[v, u] / f_y
#     z = depth[v, u]




# Generate point cloud
point_cloud = cv2.reprojectImageTo3D(disparity[18], Q)

# Flatten and remove invalid points
point_cloud = point_cloud.reshape(-1, point_cloud.shape[-1])
valid_mask = np.logical_and(~np.isnan(point_cloud[:, 0]), ~np.isinf(point_cloud[:, 0]))
point_cloud = point_cloud[valid_mask]

# Create Open3D point cloud
pcl = o3d.geometry.PointCloud()
pcl.points = o3d.utility.Vector3dVector(point_cloud)

normals_view_params = {
    "zoom": 0.3412,
    "front": [0.4257, -0.2125, -0.8795],
    "lookat": [2.6172, 2.0475, 1.532],
    "up": [-0.0694, -0.9768, 0.2024],
    "point_show_normal": True
}
# o3d.visualization.draw_geometries([pcl], **normals_view_params)

print(Q)

# o3d.visualization.draw_geometries([pcl])

