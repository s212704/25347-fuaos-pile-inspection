import xarray as xr
import numpy as np
from datetime import datetime

import matplotlib.pyplot as plt
from matplotlib.animation import ArtistAnimation

import polarTransform

from skimage.feature import blob_dog, blob_log, blob_doh
from skimage.filters import try_all_threshold, threshold_local, threshold_yen
from skimage.measure import CircleModel, label, regionprops
from skimage.morphology import area_closing, area_opening

# change filename to path of 'oculus_data.nc' (in case not within same directory)
file = xr.open_dataset(filename_or_obj='oculus_data.nc', engine='netcdf4')

## assign variables from file (NetCDF4)
azimuth_range = file.isel(ping=0).azimuth_range.to_numpy()*(180/np.pi) # rad to degree
# 130 degrees
frequency = file.isel(ping=0).frequency.to_numpy() # 1196808.5 Hz
nbeams = file.isel(ping=0).nbeams.to_numpy() # 512
# nsamples increases until 526 (from ~1500 onwards), stays constant until end
nsamples_all = file.nsamples.to_numpy()
# define for 1500 sample
nsamples = file.isel(ping=1500).nsamples.to_numpy()
# sample_size changes (i.e. decreases) gradually with ping number
# 0.019295811893439048
sample_size_all = file.sample_size.to_numpy()
sample_size = file.isel(ping=1500).sample_size.to_numpy()
# times
start_time = datetime.fromtimestamp(np.round(file.isel(ping=0).time.to_numpy()))# 2024-03-19 15:31:17
end_time = datetime.fromtimestamp(np.round(file.isel(ping=-1).time.to_numpy()))# 2024-03-19 15:42:30

## create grid & backscatter data
azimuth_offset = (180-azimuth_range)/2
x = np.radians(np.linspace(azimuth_offset,azimuth_range+azimuth_offset,nbeams))
y = np.arange(0,nsamples*sample_size,sample_size)
X, Y = np.meshgrid(x, y)

ping_selected = 4860 #1500

Z = file.isel(ping=ping_selected).backscatter.to_numpy()
polarImage = Z
#np.save('sonar',Z)
#print(Z)

time = datetime.fromtimestamp(np.round(file.isel(ping=ping_selected).time.to_numpy()))
print(time)

fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))

ax.contourf(X, Y, Z)
ax.set_thetamin(azimuth_offset)
ax.set_thetamax(azimuth_range+azimuth_offset)

# comment back in for animation
## animation of selected frame interval
# ims = []
# for i in range(4800,5000):
#  im = ax.contourf(X, Y, file.isel(ping=i).backscatter.to_numpy())
#  add_arts = im.collections
#  ims.append(add_arts)
# animation = ArtistAnimation(fig, ims)


## (1) transform polar image data to cartesian

# define function
def convertPolarFLSimageToCartesian(image, azimuth, center="middle-middle"):
    image_transformed = image.T # from Fortran to C order
    num_angles, num_samples = image.shape

    center = "middle-middle"
    initialRadius = 0
    finalRadius = num_samples

    if azimuth >= 180:
        return print("Invalid input parameter. Azimuth must be less than 180 degrees.")
    
    azimuth_offset = (180-azimuth)/2
    upper_azimuth = 180-azimuth_offset
    azimuth_min = azimuth_offset * np.pi / 180.0
    azimuth_max = upper_azimuth * np.pi / 180.0

    cartesianImage, settings = polarTransform.convertToCartesianImage(image_transformed,
                                                            center=center,
                                                            initialRadius=initialRadius,
                                                            finalRadius=finalRadius,
                                                            initialAngle=azimuth_min,
                                                            finalAngle=azimuth_max,
                                                            borderVal=0.0)
    return cartesianImage

# call function
cartesianImage = convertPolarFLSimageToCartesian(Z, azimuth_range, center="middle-middle")

# plotting polar and transformed cartesian image
fig, axes = plt.subplots(1, 2, figsize=(15, 6), sharex=True, sharey=True)
axis = axes.ravel()

axis[0].set_title('polarImage                                    ')
# (plotting backscatter data directly)
axis[0].imshow(Z) #, interpolation='nearest'
axis[0].set_axis_off()
axis[1].set_title('cartesianImage')
axis[1].imshow(cartesianImage) #, cmap='hot' #, interpolation='nearest'
axis[1].set_axis_off()

# optionally save as numpy array
# np.save('cartesian_image', cartesianImage)


## (2) binary image conversion

input_img = cartesianImage # polarImage ## for alternative method (below)

# used to try out different thresholding methods
fig, ax = try_all_threshold(input_img, figsize=(10, 8), verbose=False)
# minimum & yen seem to be best
# decided for yen, since more pronounced "banana"

global_thresh = threshold_yen(input_img)
binary_global = input_img > global_thresh

# trying local thresholding to compare to global
block_size = 35
local_thresh = threshold_local(input_img, block_size, offset=20)
binary_local = input_img > local_thresh
# yielded worse results

# get rid of all noise in the distance and around source
dist_thresh = np.round(binary_global.shape[0]/2).astype(int)
binary_global[dist_thresh:][::] = False
src_thresh = 25
binary_global[:src_thresh][::] = False

fig, axes = plt.subplots(nrows=3, figsize=(7, 8))
ax = axes.ravel()
plt.gray()

ax[0].imshow(input_img)
ax[0].set_title('Original')

ax[1].imshow(binary_global)
ax[1].set_title('Global thresholding')

ax[2].imshow(binary_local)
ax[2].set_title('Local thresholding')

for a in ax:
    a.set_axis_off()

binary_image = binary_global
# np.save('binary_image', binary_image)


## (3) clean image using morphology
morph_image = area_closing(area_opening(binary_image, 22), 20)
# opening removes bright areas smaller than
# closing removes dark areas smaller than
fig, axes = plt.subplots(1,2, figsize=(15, 7), sharex=True, sharey=True)
ax = axes.ravel()

ax[0].imshow(binary_image)
ax[0].set_title('Original')

ax[1].imshow(morph_image)
ax[1].set_title('Morphed')

# np.save('morphed_image', morph_image)


## (4) blob detection algorithms
# , num_sigma=10
blobs_log = blob_log(morph_image, max_sigma=20, threshold=.1) #.1
blobs_log[:, 2] = blobs_log[:, 2] * np.sqrt(2)

blobs_dog = blob_dog(morph_image, max_sigma=20, threshold=.1) #.1
blobs_dog[:, 2] = blobs_dog[:, 2] * np.sqrt(2)

blobs_doh = blob_doh(morph_image, max_sigma=20, threshold=.001) #.001

blobs_lists = [blobs_log, blobs_dog, blobs_doh]
colors = ['blue', 'lime', 'red']
titles = ['Laplacian of Gaussian', 'Difference of Gaussian', 'Determinant of Hessian']

# plot blob detection results
fig, axes = plt.subplots(1, 4, figsize=(15, 6), sharex=True, sharey=True)
axis = axes.ravel()

axis[0].set_title('morphed_binary_image')
axis[0].imshow(morph_image) #, interpolation='nearest'
axis[0].set_axis_off()


# plot circle and number for each detected blob
point_xy_coords = [[],[],[]] #np.zeros(shape=(len(blobs_lists),2))
for idx, (blobs, color, title) in enumerate(zip(blobs_lists, colors, titles)):
    axis[idx+1].set_title(title)
    axis[idx+1].imshow(morph_image, interpolation='nearest')
    count = 0
    for blob in blobs:
        y, x, r = blob
        point_xy_coords[idx].append([x, y])
        circle = plt.Circle((x, y), r, color=color, linewidth=2, fill=False, label=f'{count}')
        count += 1
        axis[idx+1].add_patch(circle)
        axis[idx+1].annotate(f'{count}',(x,y),color='red')

    axis[idx+1].set_axis_off()

# print('point_xy_coords',point_xy_coords)
model = CircleModel()
for i in range(0,len(blobs_lists)):
    model_success = model.estimate(np.array(point_xy_coords[i]))
    print(model_success)
    if model_success:
        circle_params = tuple(np.round(model.params, 5))
        print('point_xy_coords',circle_params) #Circle model parameters in the following order xc, yc, r.
        circle = plt.Circle((circle_params[0], circle_params[1]), circle_params[2], color='cyan', linewidth=2, fill=False)
        axis[i + 1].add_patch(circle)
    else:
        continue
# r = (6.98853, 6.63513) #6,81183

# # --- alternative method ---
# ## taking point_xy_coords from polar image
# # convert to cartesian coordinates
# # take as input for circle model

# # extract polar values from meshgrid using pixel coordinates
# point_polar_coords = [[],[],[]]
# # origin_offset = np.round(X.shape[0]/2).astype(int)
# for i in range(0,len(blobs_lists)):
#     for xy in point_xy_coords[i]:
#         #print(X.shape)
#         point_polar_coords[i].append([X[np.round(xy[0]).astype(int)][np.round(xy[1]).astype(int)],Y[np.round(xy[0]).astype(int)][np.round(xy[1]).astype(int)]])

# # convert polar values into cartesian
# # x = r × cos( θ )
# # y = r × sin( θ )
# point_cartesian_coords = [[],[],[]]
# for i in range(0,len(blobs_lists)):
#     for polar_xy in point_polar_coords[i]:
#         point_cartesian_coords[i].append([(np.cos(polar_xy[0])*polar_xy[1]),(np.sin(polar_xy[0])*polar_xy[1])])

# # use cartesian coords as input for circle model
# print('point_cartesian_coords',point_cartesian_coords)
# model = CircleModel()
# circle_params_all = []
# for i in range(0,len(blobs_lists)):
#     print(model.estimate(np.array(point_cartesian_coords[i])))
#     circle_params = tuple(np.round(model.params, 5))
#     print('point_cartesian_coords',circle_params) #Circle model parameters in the following order xc, yc, r.
#     circle_params_all.append(tuple(np.round(model.params, 5)))
#     circle = plt.Circle((circle_params[0], circle_params[1]), circle_params[2], color='cyan', linewidth=2, fill=False)
#     axis[i + 1].add_patch(circle)
# # r = (0.60249, 0.68344) #0,642965
# # higher variability, thus not used in the results

# print('polar coords:',point_polar_coords)
# print(point_xy_coords)
# # --- end of alternative ---

# print(morph_image.shape)
plt.show()



## taking point_xy_coords from polar image
# convert to cartesian coordinates
# take as input for circle model

# extract polar values from meshgrid using pixel coordinates
# point_polar_coords = [[],[],[]]
# # origin_offset = np.round(X.shape[0]/2).astype(int)
# for i in range(0,len(blobs_lists)):
#     for xy in point_xy_coords[i]:
#         #print(X.shape)
#         point_polar_coords[i].append([X[np.round(xy[0]).astype(int)][np.round(xy[1]).astype(int)],Y[np.round(xy[0]).astype(int)][np.round(xy[1]).astype(int)]])

# convert polar values into cartesian
# x = r × cos( θ )
# y = r × sin( θ )
# point_cartesian_coords = [[],[],[]]
# for i in range(0,len(blobs_lists)):
#     for polar_xy in point_polar_coords[i]:
#         point_cartesian_coords[i].append([(np.cos(polar_xy[0])*polar_xy[1]),(np.sin(polar_xy[0])*polar_xy[1])])


# print('point_cartesian_coords',point_cartesian_coords)
# model = CircleModel()
# circle_params_all = []
# for i in range(0,len(blobs_lists)):
#     print(model.estimate(np.array(point_cartesian_coords[i])))
#     print('point_cartesian_coords',tuple(np.round(model.params, 5))) #Circle model parameters in the following order xc, yc, r.
#     circle_params_all.append(tuple(np.round(model.params, 5)))
    # circle = plt.Circle((circle_params[0], circle_params[1]), circle_params[2], color='cyan', linewidth=2, fill=False)
    # axis[i + 1].add_patch(circle)
# r = (0.60249, 0.68344) #0,642965

# print('polar coords:',point_polar_coords)
# print(point_xy_coords)





## label connected components in the image
# connected component analysis

# labeled_image = label(morph_image)
# regions = regionprops(labeled_image)

# fig, axes = plt.subplots(1, 2, figsize=(15, 6), sharex=True, sharey=True)
# axis = axes.ravel()

# axis[0].set_title('caretsianImage')
# axis[0].imshow(cartesianImage) #, interpolation='nearest'
# axis[0].set_axis_off()
# axis[1].set_title('labeled')
# axis[1].imshow(labeled_image) #, interpolation='nearest'
# dias = []
# centroids = []
# for props in regions:
#     d = props.equivalent_diameter_area
#     dias.append(d)
#     cent = props.centroid
#     centroids.append(cent)
#     circle = plt.Circle((cent[1],cent[0]), d, color='cyan', linewidth=2, fill=False)
#     axis[1].add_patch(circle)
#     print(d)
# axis[1].set_axis_off()

# plt.show()

# def _blob_analysis(mask,minblobarea): #self,
#     labels = measure.label(mask, background=False)
#     properties_list = measure.regionprops(labels, cache=True)
#     filtered_list = []
#     for i, properties in enumerate(properties_list):
#         if properties.area < minblobarea: #self._
#             mask = np.where(labels == i + 1, False, mask)
#             labels = np.where(labels == i + 1, 0, labels)
#         continue
#         filtered_list.append(properties)
#     return labels, mask, filtered_list
#
# labels, mask, filtered_list = _blob_analysis(binary_image,5)
# print('labels',labels, 'filteredlist',filtered_list)